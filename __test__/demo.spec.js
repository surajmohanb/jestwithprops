import React from "react";
import { mount, shallow } from "enzyme"
import renderComponent from 'recompose/renderComponent';
import compose from 'recompose/compose';
import withState from 'recompose/withState';
import withProps from 'recompose/withProps';
import branch from 'recompose/branch'
import TnC from '../src/components/TnC';
import withHandlers  from 'recompose/withHandlers'

const shallowRenderFactory = (Component, defaultProps = {}, defaultOptions = {}) =>
  (props = defaultProps, options = defaultOptions) => shallow(<Component {...props} />, options);

//jest.mock('recompose');
jest.mock('recompose/compose');
jest.mock('recompose/withState', () => jest.fn().mockReturnValue('component with state'));
jest.mock('recompose/withHandlers', () => jest.fn().mockReturnValue('component with handlers'));
jest.mock('recompose/withProps', () => jest.fn().mockReturnValue('component with props'));
jest.mock('recompose/branch', () => jest.fn().mockReturnValue('component with branch'));


const composeHocMock = jest.fn().mockReturnValue('composed components');
compose.mockReturnValue(composeHocMock);

let props = {
  htmlContentOnly: false,
  someHtmlContent: "Some Html Content"
};
describe("Testing Compose -- Recompose", () => {
  const { default: InjectedTerms, withHandlersRefs, withPropsRef, branchRefs} = require('../src/components/App');
  it("WithProps", () => {
    //const wrapper = shallowRenderFactory(TnC, props);
    expect(InjectedTerms).toEqual('composed components');
    expect(compose).toHaveBeenCalledTimes(1);
    expect(compose)
      .toHaveBeenCalledWith(
        'component with state',
        'component with handlers',
        'component with props',
        'component with branch'
      );
    expect(composeHocMock).toHaveBeenCalledTimes(1);
    expect(composeHocMock).toHaveBeenCalledWith(TnC);
    expect(withState).toHaveBeenCalledTimes(1);
    expect(withState).toHaveBeenCalledWith("counter", "updateCounter", 0);
    expect(withHandlers).toHaveBeenCalledTimes(1);
    expect(withHandlers).toHaveBeenCalledWith(withHandlersRefs);
    expect(withProps).toHaveBeenCalledTimes(1);
    expect(withProps).toHaveBeenCalledWith(withPropsRef);
    expect(branch).toHaveBeenCalledTimes(1);

    const propCheck = withPropsRef(props);
   
    expect(propCheck).toEqual({ content: "Some Html Content" });

    const branchCheck = branchRefs(props);
    expect(branchCheck).toEqual(false);

    // withHandlersRefs().updateCounter(firstInstance);
    //expect(firstInstance.props.updateCounter).toHaveBeenCalledTimes(1);
    //expect(branch).toHaveBeenCalledWith(branchRefs);
    //expect(withState).toHaveBeenCalledWith("counter", "updateCounter", 0);
    //expect(compose).toHaveBeenCalledWith('test');
  });
  /*it("WithProps", () => {
    const htmlContent = wrapper.find("HtmlContent");
    expect(htmlContent.length).toBe(1);
    expect(htmlContent.props().content).toEqual("Some Html Content");
  });*/
  /*it("Without -- WithProps", () => {
    props = Object.assign({}, props, { htmlContentOnly: false })
    wrapper = mount(<App {...props} />);
    const button = wrapper.find("button");
    expect(button.length).toBe(1);
    button.simulate('click')
  });*/
});
