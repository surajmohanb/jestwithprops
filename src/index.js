import React from "react";
import ReactDOM from "react-dom";
import Wrapper from "./components/App";

const rootElement = document.getElementById("app");
const props = {
  htmlContentOnly: false,
  someHtmlContent: "Some Html Content"
};

ReactDOM.render(<Wrapper {...props} />, rootElement);
