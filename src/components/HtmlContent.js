import React, { Component, Fragment } from "react";

const HtmlContent = ({ content }) => {
  return (
    <Fragment>
      <h2>HTML Content Will Show</h2>
      <div>{content}</div>
      <span>Some SPAN Content</span>
    </Fragment>
  );
};

export default HtmlContent;
