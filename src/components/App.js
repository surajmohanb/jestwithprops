import React from "react";
import TnC from "./TnC";
import HtmlContent from "./HtmlContent";
import renderComponent from 'recompose/renderComponent';
import compose from 'recompose/compose';
import withState from 'recompose/withState';
import withProps from 'recompose/withProps';
import branch from 'recompose/branch';
import withHandlers  from 'recompose/withHandlers'

export const withHandlersRefs = () => {
  return {
    setCounter: ({ counter, updateCounter }) => {
      return event => {
        counter = counter + 3;
        updateCounter(counter);
      };
    }
  }
}

export const withPropsRef = val => {
  return { content: val.someHtmlContent };
};

export const branchRefs = val => val.htmlContentOnly

export default compose(
  withState("counter", "updateCounter", 0),
  withHandlers(withHandlersRefs),
  withProps(withPropsRef),
  branch(branchRefs,renderComponent(HtmlContent))
)(TnC);
