import React, { Component, Fragment } from "react";

const TermsAndConditions = ({ counter, setCounter }) => {
  return (
    <Fragment>
      <button onClick={setCounter}>Click Me</button>
      <div>{counter}</div>
    </Fragment>
  );
};

export default TermsAndConditions;
